//
//  Constants.swift
//  StartMeetingTask
//
//  Created by Anton Shevlyagin on 13/11/2017.
//  Copyright © 2017 Anton Shevlyagin. All rights reserved.
//

import Foundation

struct Constants {
    struct Events {
        static let LIST_URL: String = "https://kudago.com/public-api/v1.3/events/"
        static let DETAIL_URL: String = "https://kudago.com/public-api/v1.3/events/"
        static let LIST_NODE_KEY: String = "results"
        static let NEXT_PAGE_NODE_KEY: String = "next"
        static let LIST_FIELDS = "?fields="
        static let LIST_DATE = "&dates="
        static let LIST_ID = "id"
        static let LIST_IMAGES = "images"
        static let LIST_SHORT_TEXT = "description"
        static let LIST_TITLE = "title"
    }

    struct Cities {
        static let LIST_URL: String = "https://kudago.com/public-api/v1.3/locations/"
        static let NAME_KEY: String = "name"
        static let SLUG_KEY: String = "slug"
    }
}

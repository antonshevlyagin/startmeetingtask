//
//  ImageLoader.swift
//  StartMeetingTask
//
//  Created by Anton Shevlyagin on 13/11/2017.
//  Copyright © 2017 Anton Shevlyagin. All rights reserved.
//

import UIKit

func createImageCache() -> NSCache<NSString, UIImage> {
    let cache: NSCache<NSString, UIImage> = NSCache<NSString, UIImage>()
    cache.totalCostLimit = 32*1000
    
    return cache
}

class ImageLoader: ImageLoaderProtocol {
    var completionQueue: DispatchQueue
    static let imageCache = createImageCache()
    var loadTasks:[URLSessionTask] = [URLSessionTask]()
    
    init(completionQueue: DispatchQueue) {
        self.completionQueue = completionQueue
    }
    
    func loadImage(url: URL, size: CGSize?, completion: @escaping (UIImage?) -> Void) {
        guard self.loadTasks.index(where: { $0.originalRequest?.url == url }) == nil else {
            return
        }
        
        if let cachedImage = ImageLoader.imageCache.object(forKey: url.absoluteString as NSString) {
            self.completionQueue.async { completion(cachedImage) }
        } else {
            loadData(url: url) { data, response, error in
                guard let data = data, error == nil else {
                    self.completionQueue.async { completion(nil) }
                    return
                }
                
                let recievedImage = UIImage(data: data)
                guard var image = recievedImage else {
                    self.completionQueue.async { completion(nil) }
                    return
                }

                let cost: Int = Int(image.size.height * image.size.width/(1000)) // like kpix
                ImageLoader.imageCache.setObject(image, forKey: url.absoluteString as NSString, cost: cost)
                
                if let size = size {
                    image = self.resizeImage(image: image, newSize: size) ?? image
                }
                
                self.completionQueue.async { completion(image) }
            }
        }
    }
    
    private func loadData(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
        }
        
        task.resume()
        self.loadTasks.append(task)
    }
    
    private func resizeImage(image: UIImage, newSize: CGSize) -> UIImage? {
        let scaleWidth = newSize.width/image.size.width
        let scaleHeight = newSize.height/image.size.height
        let minScale = min(scaleWidth, scaleHeight)
        UIGraphicsBeginImageContext(CGSize(width: minScale*image.size.width, height: minScale*image.size.height))
        image.draw(in: CGRect(x: 0, y: 0, width: minScale*image.size.width, height: minScale*image.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func cancelLoadImage(url: URL) {
        guard let taskIdx = self.loadTasks.index(where: { $0.originalRequest?.url == url }) else {
            return
        }
        
        let task = self.loadTasks[taskIdx]
        // note: при отмене вызывается делегат с кодом ошибки -999 (https://developer.apple.com/documentation/foundation/urlsessiontask/1411591-cancel) и пишется в лог, это ОК - потом загрузим заново
        task.cancel()
        self.loadTasks.remove(at: taskIdx)
    }
}

//
//  ImageLoaderProtocol.swift
//  StartMeetingTask
//
//  Created by Anton Shevlyagin on 13/11/2017.
//  Copyright © 2017 Anton Shevlyagin. All rights reserved.
//

import Foundation
import UIKit

protocol ImageLoaderProtocol {
    // note: добавил параметр size - ко многим статьям картинки идут под 10 мпикс
    func loadImage(url: URL, size: CGSize?, completion: @escaping (_ image: UIImage?) -> Void)
    func cancelLoadImage(url: URL)
}

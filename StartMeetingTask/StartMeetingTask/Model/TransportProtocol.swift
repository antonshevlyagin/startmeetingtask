//
//  TransportProtocol.swift
//  StartMeetingTask
//
//  Created by Anton Shevlyagin on 13/11/2017.
//  Copyright © 2017 Anton Shevlyagin. All rights reserved.
//

import Foundation

public enum TransportError: Error {
    case LoadDataFailedError
    case LoadDataFormatError
    case LoadDataUpdateError
    case LoadDataInvalidParam
}

extension TransportError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .LoadDataFailedError:
            return NSLocalizedString("Load data with news failed.", comment: "")
        case .LoadDataFormatError:
            return NSLocalizedString("Format data invalid.", comment: "")
        case .LoadDataUpdateError:
            return NSLocalizedString("Unable load data from server.", comment: "")
        case .LoadDataInvalidParam:
            return NSLocalizedString("Invalid param.", comment: "")
        }
    }
}

protocol TransportProtocol {
    func loadEventsList(date: Date, page: URL?, completion: @escaping (_ error: Error?, _ eventsList: [Event], _ nextPage: URL?) -> Void)
    func loadEvent(event: Event, completion: @escaping (_ error: Error?, _ event: EventDetail?) -> Void)
    func stopLoading()
}

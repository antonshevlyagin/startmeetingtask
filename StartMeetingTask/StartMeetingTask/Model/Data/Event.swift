//
//  Event.swift
//  StartMeetingTask
//
//  Created by Anton Shevlyagin on 13/11/2017.
//  Copyright © 2017 Anton Shevlyagin. All rights reserved.
//

import UIKit

class Event: NSObject {
    var id: String?
    var title: String?
    var shortText: String?
    var images: [URL] = [URL]()

    var firstImage: URL? {
        get {
            if images.count > 0 {
                return images.first
            } else {
                return nil
            }
        }
    }
    
    enum EventKeys: String {
        case title = "title"
        case id = "id"
        case shortText = "description"
        case image = "images"
    }
}

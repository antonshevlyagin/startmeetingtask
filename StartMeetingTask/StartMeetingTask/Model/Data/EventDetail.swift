//
//  EventDetail.swift
//  StartMeetingTask
//
//  Created by Anton Shevlyagin on 13/11/2017.
//  Copyright © 2017 Anton Shevlyagin. All rights reserved.
//

import UIKit

class EventDetail: NSObject {
    var location: String?
    var text: String?
    var images: [URL] = [URL]()
    var shortText: String?
    var price: String = "free"
    var tags: [String] = [String]()
    
    enum EventDetailKeys: String {
        case location = "location"
        case locationKey = "slug"
        case text = "body_text"
        case price = "price"
        case images = "images"
        case image = "image"
        case shortText = "description"
        case tag = "categories"
    }
}

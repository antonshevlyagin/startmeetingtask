//
//  String+HTML.swift
//  StartMeetingTask
//
//  Created by Anton Shevlyagin on 14/11/2017.
//  Copyright © 2017 Anton Shevlyagin. All rights reserved.
//

import Foundation
import UIKit

extension String {
    var attributed: NSAttributedString {
        get {
            guard let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false) else {
                return NSAttributedString(string: self)
            }
            
            guard let attribited = try? NSMutableAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil) else {
                return NSAttributedString(string: self)
            }
            
            return attribited
        }
    }
}

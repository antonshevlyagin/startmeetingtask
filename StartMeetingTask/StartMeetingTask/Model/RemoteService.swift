//
//  TransportService.swift
//  StartMeetingTask
//
//  Created by Anton Shevlyagin on 13/11/2017.
//  Copyright © 2017 Anton Shevlyagin. All rights reserved.
//

import Foundation

func createUrlSession() -> URLSession {
    let sessionConfig = URLSessionConfiguration.default
    sessionConfig.httpMaximumConnectionsPerHost = 2
    return URLSession(configuration: sessionConfig)
}

class RemoteService: TransportProtocol {
    var completionQueue: DispatchQueue
    let session = createUrlSession()
    var citiesDic:[String: String]?
    var loadTasks:[URLSessionTask] = [URLSessionTask]()
    
    init(completionQueue: DispatchQueue) {
        self.completionQueue = completionQueue
    }
    
    func loadEventsList(date: Date, page: URL?, completion: @escaping (Error?, [Event], URL?) -> Void) {
        var pageUrl:URL
        
        if let page = page {
            pageUrl = page
        } else {
            if let url = URL(string: Constants.Events.LIST_URL
                + Constants.Events.LIST_FIELDS + Constants.Events.LIST_ID
                + "," + Constants.Events.LIST_IMAGES
                + "," + Constants.Events.LIST_TITLE
                + "," + Constants.Events.LIST_SHORT_TEXT
                + Constants.Events.LIST_DATE
                + String(format:"%.0f", date.timeIntervalSince1970)) {
                pageUrl = url
            } else {
                self.completionQueue.async {completion(TransportError.LoadDataInvalidParam, [], nil)}
                return
            }
        }
        
        DispatchQueue.global().async {
            self.getData(url: pageUrl) { (responceData, error) in
                guard let data = responceData else {
                    self.completionQueue.async {completion(error, [], nil)}
                    return
                }
                
                do {
                    guard let jsonDic = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
                        self.completionQueue.async {completion(TransportError.LoadDataFormatError, [], nil)}
                        return
                    }
                    
                    var nextPageUrl:URL?
                        
                    if let nextPage = jsonDic[Constants.Events.NEXT_PAGE_NODE_KEY] as? String {
                        nextPageUrl = URL(string: nextPage)
                    }
                    
                    if let jsonEventsList = jsonDic[Constants.Events.LIST_NODE_KEY] as? [[String: Any]] {
                        let eventsList = self.extractEventsList(jsonDicList: jsonEventsList)
                        
                        if (eventsList.count != jsonEventsList.count) {
                            self.completionQueue.async {completion(TransportError.LoadDataFormatError, [], nil)}
                            return
                        }
                        
                        self.completionQueue.async { completion(nil, eventsList, nextPageUrl) }
                        return
                    }
                    
                    self.completionQueue.async {completion(TransportError.LoadDataFormatError, [], nil)}
                } catch {
                    self.completionQueue.async {completion(TransportError.LoadDataFormatError, [], nil)}
                }
            }
        }
    }
    
    private func loadCities(completion: @escaping ([String: String]?, Error?) -> Void) {
        DispatchQueue.global().async {
            self.getData(url: URL(string: Constants.Cities.LIST_URL)!) { (responceData, error) in
                guard let data = responceData else {
                    completion(nil, error)
                    return
                }
                
                do {
                    guard let jsonDicList = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: String]] else {
                        completion(nil, TransportError.LoadDataFormatError)
                        return
                    }
                    
                    var result: [String: String] = [String: String]()
                    
                    for jsonDic in jsonDicList {
                        if let name = jsonDic[Constants.Cities.NAME_KEY],
                        let slug = jsonDic[Constants.Cities.SLUG_KEY] {
                            result.updateValue(name, forKey:slug)
                        }
                    }
                    
                    completion(result, nil)
                } catch {
                    completion(nil, TransportError.LoadDataFailedError)
                }
            }
        }
    }
    
    private func getData(url: URL, completion: @escaping (Data?, Error?) -> Void) {
        guard self.loadTasks.index(where: { $0.originalRequest?.url == url }) == nil else {
            return
        }
        
        let task = self.session.dataTask(with: url) {
            (data, response, error) in
            if let error = error as? URLError,
                error.code == URLError.Code.cancelled{
                // nothing
                return
            }
            guard let responseData = data else {
                completion(nil, error)
                return
            }
            
            completion(responseData, error)
        }
        
        task.resume()
        self.loadTasks.append(task)
    }
    
    
    private func extractEventsList(jsonDicList: [[String: Any]]) -> [Event] {
        var result: Array = [Event]()
        
        for ele in jsonDicList {
            if let event = self.jsonToEvent(jsonDic: ele) {
                result.append(event)
            }
        }
        
        return result
    }
    
    private func jsonToEvent(jsonDic: [String: Any]) -> Event? {
        if let idNum = jsonDic[Event.EventKeys.id.rawValue] as? Double,
            let title = jsonDic[Event.EventKeys.title.rawValue] as? String,
            let shortText = jsonDic[Event.EventKeys.shortText.rawValue] as? String {
                let result: Event = Event()
                result.id = String(format:"%.0f", idNum)
                result.title = title
                result.shortText = shortText
            
            if let imagesDic = jsonDic[Event.EventKeys.image.rawValue] as? [[String: Any]] {
                result.images = self.parseImagesUrl(dic: imagesDic)
            }
            
            return result
        }

        return nil
    }
    
    private func jsonToEventDetail(jsonDic: [String: Any]) -> EventDetail {
        let result: EventDetail = EventDetail()
        result.text = jsonDic[EventDetail.EventDetailKeys.text.rawValue] as? String
        result.tags =  jsonDic[EventDetail.EventDetailKeys.tag.rawValue] as? [String] ?? []
        result.shortText = jsonDic[EventDetail.EventDetailKeys.shortText.rawValue] as? String
        
        if let locDic: [String: String] = jsonDic[EventDetail.EventDetailKeys.location.rawValue] as? [String: String],
            let loc = locDic[EventDetail.EventDetailKeys.locationKey.rawValue],
            let locaton = self.citiesDic?[loc] {
            result.location = locaton
        }
        
        if let price = (jsonDic[EventDetail.EventDetailKeys.price.rawValue] as? String),
            price.count > 0 {
            result.price = price
        }
        
        if let imagesDic = jsonDic[EventDetail.EventDetailKeys.images.rawValue] as? [[String: Any]] {
            result.images = self.parseImagesUrl(dic: imagesDic)
        }
        
        return result
    }
    
    func loadEvent(event: Event, completion: @escaping (_ error: Error?, _ event: EventDetail?) -> Void) {
        DispatchQueue.global().async {
            let url: URL = URL(string: Constants.Events.DETAIL_URL + event.id! + "/")!
            self.getData(url: url) { (responceData, error) in
                guard let data = responceData else {
                    self.completionQueue.async {completion(error, nil)}
                    return
                }
                
                do {
                    guard let jsonDic = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
                        self.completionQueue.async {completion(TransportError.LoadDataFormatError, nil)}
                        return
                    }
                    
                    let completionBlock: (() -> Void) = {
                        let eventDetail: EventDetail = self.jsonToEventDetail(jsonDic: jsonDic)
                        self.completionQueue.async {completion(nil, eventDetail)}
                    }
                    
                    if self.citiesDic == nil {
                        self.loadCities(completion: { (citiesDic, error) in
                            self.citiesDic = citiesDic
                            completionBlock()
                        })
                    } else {
                        completionBlock()
                    }
                } catch {
                    self.completionQueue.async {completion(TransportError.LoadDataFailedError, nil)}
                }
            }
        }
    }
    
    private func parseImagesUrl(dic: [[String: Any]]) -> [URL] {
        var imagesUrls: [URL] = [URL]()
            
        for imagesDic in dic {
            if let urlText = imagesDic[EventDetail.EventDetailKeys.image.rawValue] as? String,
                let url = URL(string: urlText){
                imagesUrls.append(url)
            }
        }
        
        return imagesUrls
    }
    
    func stopLoading() {
        for (idx, task) in self.loadTasks.enumerated() {
            // note: при отмене вызывается делегат с кодом ошибки -999 (https://developer.apple.com/documentation/foundation/urlsessiontask/1411591-cancel) и пишется в лог
            task.cancel()
            self.loadTasks.remove(at: idx)
        }
    }
}

//
//  EventDetailViewController.swift
//  StartMeetingTask
//
//  Created by Anton Shevlyagin on 13/11/2017.
//  Copyright © 2017 Anton Shevlyagin. All rights reserved.
//

import UIKit

class EventDetailViewController: UIViewController {
    @IBOutlet var detaiTtextView: UITextView!
    var event: Event?
    var eventDetail: EventDetail?
    var activityIndicator: UIActivityIndicatorView!
    let eventService: TransportProtocol = RemoteService(completionQueue: DispatchQueue.main)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addActivityIndicator()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.loadDetail()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPageViewController" ,
            let nextScene = segue.destination as? PageViewController {
            nextScene.photoUrls = self.event?.images
        }
    }
    
    private func loadDetail() {
        if let event = self.event {
            self.activityIndicator.startAnimating()
            self.eventService.loadEvent(event: event, completion: { (error, eventDetail) in
                if (error == nil) {
                    self.eventDetail = eventDetail
                    self.update()
                } else {
                    self.showError(error!)
                }
                
                self.activityIndicator.stopAnimating()
            })
        }
    }
    
    private func update() {
        self.title = event?.title
        
        let mainText = self.eventDetail?.text ?? "unknown"
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(attributedString: mainText.attributed)
        let priceText = self.eventDetail?.price ?? "unknown"
        let price: NSMutableAttributedString = NSMutableAttributedString(string: "price: " + priceText + "\n\n")
        var tags: String = String()
        
        if let tagsList = self.eventDetail?.tags {
            for tag in tagsList {
                if (tags.count == 0) {
                    tags = tag
                } else {
                    tags = tags + " , " + tag
                }
            }
        }

        let tag: NSMutableAttributedString = NSMutableAttributedString(string: "tags: " + tags + "\n")
        let location = self.eventDetail?.location ?? "unknown"
        let text: NSMutableAttributedString = NSMutableAttributedString(string: "location: " + location + "\n")
        
        text.append(tag)
        text.append(price)
        text.append(attributedString)
        
        self.detaiTtextView.attributedText = text
    }
    
    private func addActivityIndicator() {
        self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        self.activityIndicator.color = UIColor.blue
        let barButtonItem: UIBarButtonItem = UIBarButtonItem(customView: activityIndicator)
        self.navigationItem.rightBarButtonItem = barButtonItem
    }
    
    private func showError(_ error: Error) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

//
//  ViewController.swift
//  StartMeetingTask
//
//  Created by Anton Shevlyagin on 13/11/2017.
//  Copyright © 2017 Anton Shevlyagin. All rights reserved.
//

import UIKit

class EventsListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, DateSelectorViewDelegate {
    @IBOutlet var eventsTable: UITableView!
    var activityIndicator: UIActivityIndicatorView!
    var events: [Event] = [Event]()
    let eventService: TransportProtocol = RemoteService(completionQueue: DispatchQueue.main)
    let dateFormatter: DateFormatter = DateFormatter()
    let sliceTimeInterval: TimeInterval  = 24*60*60 // day
    var currentPageURL: URL? = nil
    var allEventsIsLoaded: Bool = false
    
    var date: Date? {
        didSet {
            self.allEventsIsLoaded = false
            self.events.removeAll()
            self.eventService.stopLoading()
            self.updateTable()
            self.update()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addActivityIndicator()
        self.date = Date()
        let dateSelectorView = DateSelectorView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        dateSelectorView.delegate = self
        self.navigationItem.titleView = dateSelectorView
        
        self.setupTable()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowEventDetail" ,
            let nextScene = segue.destination as? EventDetailViewController ,
            let indexPath = self.eventsTable?.indexPathForSelectedRow {
            nextScene.event = self.events[indexPath.row]
        }
    }
    
    // DateSelectorViewDelegate
    
    func dateChanged(newDate: Date) {
        self.date = newDate
    }
    
    // UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: indexPath) as! EventTableViewCell
        
        let event: Event = self.events[indexPath.row]
        
        cell.shortTextLabel.attributedText = event.shortText?.attributed
        cell.titleLabel.text = event.title
        cell.eventImageView.image = nil
        cell.imageUrl = event.firstImage
        cell.startImageFromUrl()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell = cell as! EventTableViewCell
        cell.stopLoadImageFromUrl()
    }
    
    // Private
    
    private func dateToText(_ date: Date) -> String {
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        return dateFormatter.string(from: date)
    }
    
    private func addActivityIndicator() {
        self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        self.activityIndicator.color = UIColor.blue
        let barButtonItem: UIBarButtonItem = UIBarButtonItem(customView: activityIndicator)
        self.navigationItem.rightBarButtonItem = barButtonItem
    }
    
    private func setupTable() {
        self.eventsTable.dataSource = self
        self.eventsTable.delegate = self
        self.eventsTable.tableFooterView = UIView()
        self.eventsTable.rowHeight = UITableViewAutomaticDimension
        self.eventsTable.estimatedRowHeight = 64
    }
    
    private func update() {
        self.activityIndicator.startAnimating()
        
        self.eventService.loadEventsList(date: self.date!, page: self.currentPageURL) { (error, loadedEvents, nextPageUrl) in
            if (error == nil) {
                self.events.append(contentsOf: loadedEvents)
                self.updateTable()
                
                if ((self.currentPageURL != nil) && (nextPageUrl == nil)) {
                    self.allEventsIsLoaded = true
                }
                
                self.currentPageURL = nextPageUrl
            } else {
                self.showError(error!)
            }
            
            self.activityIndicator.stopAnimating()
        }
    }
    
    private func updateTable() {
        self.eventsTable.reloadData()
    }
    
    private func showError(_ error: Error) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}


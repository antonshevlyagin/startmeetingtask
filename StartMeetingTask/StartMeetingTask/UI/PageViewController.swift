//
//  PageViewController.swift
//  StartMeetingTask
//
//  Created by Anton Shevlyagin on 14/11/2017.
//  Copyright © 2017 Anton Shevlyagin. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    var currentPage: Int = 0
    var photoUrls: [URL]?
    
    required init?(coder: NSCoder) {
        super.init(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: .horizontal, options: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let photoViewController: PhotoViewController = self.storyboard?.instantiateViewController(withIdentifier: "PhotoViewController") as? PhotoViewController {
            photoViewController.photoUrl = self.photoUrls?[0]
            self.setViewControllers([photoViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        }
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let photoViewController: PhotoViewController = self.storyboard?.instantiateViewController(withIdentifier: "PhotoViewController") as? PhotoViewController  {
            let idx: Int = pageViewController.dataSource!.presentationIndex!(for: pageViewController)
            
            if (idx < 1) {
                return nil
            } else {
                photoViewController.photoUrl = photoUrls![idx-1]
            }
            
            return photoViewController
        } else {
            return nil
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let photoViewController: PhotoViewController = self.storyboard?.instantiateViewController(withIdentifier: "PhotoViewController") as? PhotoViewController  {
            let idx: Int = pageViewController.dataSource!.presentationIndex!(for: pageViewController)
            
            if (idx >= self.presentationCount(for: pageViewController)-1) {
                return nil
            } else {
                photoViewController.photoUrl = photoUrls![idx+1]
            }
            
            return photoViewController
        } else {
            return nil
        }
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        if let urls = photoUrls {
            return urls.count
        }
        
        return 0
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        if let urls = photoUrls,
        let photoViewController: PhotoViewController = pageViewController.viewControllers?[0] as? PhotoViewController,
        let url = photoViewController.photoUrl,
        let result = urls.index(of: url) {
            return result
        }
        
        return 0
    }
}

//
//  PhotoViewController.swift
//  StartMeetingTask
//
//  Created by Anton Shevlyagin on 14/11/2017.
//  Copyright © 2017 Anton Shevlyagin. All rights reserved.
//

import UIKit

class PhotoViewController: UIViewController {
    @IBOutlet var imageView: UIImageView!
    let imageLoader: ImageLoaderProtocol = ImageLoader(completionQueue: DispatchQueue.main)
    @IBOutlet var loadingProgress: UIActivityIndicatorView!
    var photoUrl: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (self.photoUrl != nil) {
            self.startLoadImage()
        }
    }
    
    private func startLoadImage() {
        self.loadingProgress.startAnimating()
        
        if let url = self.photoUrl {
            self.imageLoader.loadImage(url: url, size: nil, completion: { (loadedImage) in
                self.imageView.image = loadedImage
                self.loadingProgress.stopAnimating()
            })
        }
    }
}

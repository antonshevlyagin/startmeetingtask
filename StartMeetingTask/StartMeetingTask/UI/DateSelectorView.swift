//
//  DateSelectorView.swift
//  StartMeetingTask
//
//  Created by Anton Shevlyagin on 19/11/2017.
//  Copyright © 2017 Anton Shevlyagin. All rights reserved.
//

import UIKit

let btnDimension: CGFloat = 36
let dateLabelDimension: CGFloat = 240

protocol DateSelectorViewDelegate {
    func dateChanged(newDate: Date)
}

enum DateSelectorInterval {
    case day
}

class DateSelectorView: UIView {
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var contentView: UIView!
    var delegate: DateSelectorViewDelegate?
    var dateInterval: TimeInterval = 0
    var dateFormat: String = "dd.MM.yyyy"
    let dateFormatter = DateFormatter()
    let weekDaydateFormatter = DateFormatter()
    let calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
    
    var dateSelectorInterval:DateSelectorInterval = DateSelectorInterval.day {
        didSet {
            self.dateInterval = self.dayFromDateSelectorInterval()
        }
    }
    
    var date: Date = Date() {
        didSet {
            self.updateLabel()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    @IBAction func nextDateBtnHandler(_ sender: UIButton) {
        self.dateNext()
    }
    
    @IBAction func previousDateBtnHandler(_ sender: UIButton) {
        self.datePrev()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("DateSelector", owner: self, options: nil)
        self.addSubview(self.contentView)
        self.contentView.frame = self.bounds
        self.contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.dateInterval = self.dayFromDateSelectorInterval()
        self.dateFormatter.dateFormat = self.dateFormat
        self.updateLabel()
    }
    
    private func dayFromDateSelectorInterval() -> TimeInterval {
        var interval: TimeInterval
        
        switch self.dateSelectorInterval {
        case .day:
            interval = 24*60*60
        }
        
        return interval
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 2*btnDimension + dateLabelDimension, height: btnDimension)
    }
    
    private func updateLabel() {
        self.dateLabel.text = self.dateInText() + " " + self.weekDay()
    }
    
    private func dateInText() -> String {
        return self.dateFormatter.string(from: self.date)
    }
    
    private func weekDay() -> String {
        weekDaydateFormatter.dateFormat = "EEEE"
        return weekDaydateFormatter.string(from: self.date).capitalized
    }
    
    private func dateNext() {
        self.date = self.date.addingTimeInterval(self.dateInterval)
        self.updateDelegate()
    }
    
    private func datePrev() {
        self.date = self.date.addingTimeInterval(-self.dateInterval)
        self.updateDelegate()
    }
    
    private func updateDelegate() {
        if let delegate = self.delegate {
            delegate.dateChanged(newDate: self.date)
        }
    }
}

//
//  EventTableViewCell.swift
//  StartMeetingTask
//
//  Created by Anton Shevlyagin on 13/11/2017.
//  Copyright © 2017 Anton Shevlyagin. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {
    @IBOutlet var shortTextLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var eventImageView: UIImageView!
    @IBOutlet var loadingProgress: UIActivityIndicatorView!
    var imageUrl: URL?
    var isLoading: Bool = false
    let imageLoader: ImageLoaderProtocol = ImageLoader(completionQueue: DispatchQueue.main)
    
    func startImageFromUrl() {
        if let url = self.imageUrl {
            self.isLoading = true
            self.loadingProgress.startAnimating()
            
            self.imageLoader.loadImage(url: url, size: self.eventImageView.bounds.size, completion: { (loadedImage) in
                if self.isLoading  {
                    self.loadingProgress.stopAnimating()
                    self.eventImageView.image = loadedImage
                }
            })
        }
    }
    
    func stopLoadImageFromUrl() {
        self.loadingProgress.stopAnimating()
        
        if let url = self.imageUrl {
            self.imageLoader.cancelLoadImage(url: url)
        }
        
        self.isLoading = false
        self.eventImageView.image = nil
    }
}
